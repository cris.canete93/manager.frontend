export class IdentityHelper{

    static isUserAuthenticated(isTokenExpired: boolean): boolean{
        const currentTokent: string = localStorage.getItem('jwt');
        if (currentTokent && !isTokenExpired) {
            console.log('is authenticated');
            return true;
        }else {
            console.log('is not authenticated');
            return false;
        }
    }

    static logOut(): void{
        localStorage.removeItem('jwt');
    }
}
