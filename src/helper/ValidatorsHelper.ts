import { FormGroup } from '@angular/forms';

export class ValidatorsHelper{

    static matchPassword(passwordOne: string, password: string): any {
        return (formGroup: FormGroup) => {
          const passwordControl = formGroup.controls[passwordOne];
          const confirmPasswordControl = formGroup.controls[password];
          if (!passwordControl || !confirmPasswordControl) {
            return null;
          }
          if (confirmPasswordControl.errors && !confirmPasswordControl.errors.passwordMismatch) {
            return null;
          }
          if (passwordControl.value !== confirmPasswordControl.value) {
            confirmPasswordControl.setErrors({ passwordMismatch: true });
          } else {
            confirmPasswordControl.setErrors(null);
          }
        };
      }
}
