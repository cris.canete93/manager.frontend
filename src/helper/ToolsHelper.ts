export class ToolsHelper {

    static getDataError(errorData: any): string {
        console.log(errorData);
        let errors = '';
        for (const key of  Object.keys(errorData)) {
            errors = `${errorData[key]}`;
        }
        return errors;
    }
}
