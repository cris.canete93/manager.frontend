import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'typeSexPipe'
})
export class TypeSexPipe implements PipeTransform {

    transform(status: string): string {
        let statusDescripcion = '';
        switch (status) {
            case 'H':
                statusDescripcion = 'Masculino';
                break;
            case 'M':
                statusDescripcion = 'Femenino';
                break;
            default:
                statusDescripcion = 'Sin asignacion';
                break;
        }
        return statusDescripcion;
    }

}
