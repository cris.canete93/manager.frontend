import { createElementCssSelector } from '@angular/compiler';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'statustypePipe'
})
export class StatusTypePipe implements PipeTransform {

    transform(status: boolean): string {
        if (status) {
            return 'Activo ( SI )';
        }
        else {
            return 'Inactivo ( NO )';
        }
    }

}
