import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypeSexPipe } from '../pipes/manager/TypeSex.pipe';
import { StatusTypePipe } from './manager/StatusType.pipe';

@NgModule({
  declarations: [
    TypeSexPipe,
    StatusTypePipe
  ],
  exports: [
    TypeSexPipe,
    StatusTypePipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
