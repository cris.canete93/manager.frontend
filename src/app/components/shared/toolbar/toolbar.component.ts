import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { IdentityHelper } from '../../../../helper/IdentityHelper';
import { AuthService } from '../../../../services/Auth/auth.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  isAuth: boolean;

  constructor(private jwtHelper: JwtHelperService,
              private authService: AuthService,
              private router: Router) {
    this.isAuth = IdentityHelper.isUserAuthenticated(this.jwtHelper.isTokenExpired());
  }

  ngOnInit(): void {
    this.authService.isCurrentAuthentication$().subscribe( isAuth => {
      this.isAuth = isAuth;
    });
  }

  public logOutSession(): void {
    IdentityHelper.logOut();
    this.isAuth = IdentityHelper.isUserAuthenticated(this.jwtHelper.isTokenExpired());
    this.router.navigate(['login']);
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }
}
