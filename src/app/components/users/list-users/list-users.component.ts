import { Component, OnInit, AfterContentInit, ViewChild, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ToolsHelper } from '../../../../helper/ToolsHelper';
import { ToasServices } from '../../../../handler/ToasServices';
import { PageEntityViewModel } from '../../../../model/PageEntityViewModel';
import { UserViewModel } from '../../../../model/UserViewModel';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  displayedColumns: string[] = ['userId', 'email', 'userName', 'sex', 'status', 'actions'];
  dataSource = new MatTableDataSource<UserViewModel>();

  @Output() selectCurrentUser: EventEmitter<UserViewModel> = new EventEmitter<UserViewModel>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userService: UserService,
              private toast: ToasServices) {
    this.subscribeReload();
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.getUserCollection();
  }

  private subscribeReload(): void {
    this.userService.isReload$.asObservable().subscribe((reload: boolean) => {
      if (reload) {
        this.getUserCollection();
      }
    });
  }

  private getUserCollection(): void {
    const page = 1;
    this.userService.getAllUser(page)
                    .subscribe((response: PageEntityViewModel<UserViewModel>) => {
                      this.dataSource = new MatTableDataSource(response.records);
                    });
  }

  public onEditUser(row: UserViewModel): void {
    if (row !== undefined ) {
      this.selectCurrentUser.emit(row);
    }
  }

  public onDesactiveUser(row: UserViewModel): void {
    if (row !== undefined ) {
        this.userService.changeStatus(row).subscribe( (response: any) => {
                          if (response.isSuccesFully) {
                            this.toast.Success('¡Se guardo con exito!');
                            this.getUserCollection();
                          }
                          else{
                            this.toast.Success(`Ocurrio un error ${ToolsHelper.getDataError(response.errors)}`);
                          }
                        }, (error) => {
                          this.toast.Warn(`Ocurrio un error ${ToolsHelper.getDataError(error)}`);
                        });
                      }
  }
}
