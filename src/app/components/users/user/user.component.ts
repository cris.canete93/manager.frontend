import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ParamViewModel } from '../../../../model/ParamViewModel';
import { UserViewModel } from '../../../../model/UserViewModel';
import { UserAddComponent } from '../user-add/user-add.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  typeOperation: ParamViewModel;
  constructor(public dialog: MatDialog) {
    this.typeOperation = new ParamViewModel();
  }

  ngOnInit(): void {
  }

  openDialogUser(): void {
    this.typeOperation.typeOperation = 'Insert';
    this.goOpenDialog();
  }

  public processUserUpdate(user: UserViewModel): void {
    this.typeOperation.typeOperation = 'update';
    this.typeOperation.user = user;
    this.goOpenDialog();
  }

   private goOpenDialog(): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '40%';
      dialogConfig.data = this.typeOperation;
      this.dialog.open(UserAddComponent, dialogConfig);
  }
}
