import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ValidatorsHelper } from '../../../../helper/ValidatorsHelper';
import { UserService } from '../../../../services/user.service';
import { ToolsHelper } from '../../../../helper/ToolsHelper';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ParamViewModel } from '../../../../model/ParamViewModel';
import { ToasServices } from '../../../../handler/ToasServices';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  propertyOperation: string;
  formUser: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public dataType: ParamViewModel,
              private form: FormBuilder,
              private userService: UserService,
              private toast: ToasServices,
              private dialogRef: MatDialogRef<UserAddComponent>) {
    this.initFormsUser();
  }

  ngOnInit(): void {
    if (this.dataType.typeOperation !== 'Insert') {
        console.log('proces update');
        this.formUser.patchValue({
          userId: this.dataType.user?.userId,
          username: this.dataType.user?.userName,
          email: this.dataType.user?.email,
          sex: this.dataType.user?.sex,
          authId: this.dataType.user?.authId
        });
    }
  }

  private initFormsUser(): void {
    this.formUser = this.form.group({
      typeOperation: [this.dataType.typeOperation],
      userId: [''],
      authId: [''],
      sex: ['', Validators.required],
      username: ['', Validators.minLength(7)],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      onePassword: ['', Validators.compose([
        Validators.pattern(/^(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/),
        Validators.minLength(8),
        Validators.required])],
      password: ['', Validators.compose([
        Validators.pattern(/^(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/),
        Validators.minLength(8),
        Validators.required]),
    ]},
    {
      validators: [ ValidatorsHelper.matchPassword('onePassword' , 'password')],
    });
  }

  persistenceUser(): any {
      if (this.dataType.typeOperation === 'Insert') {
          this.SaveUser();
      }
      else {
        this.UpdateUser();
      }
  }

  private SaveUser(): void {
    this.userService.persistenceUser(this.formUser.value)
                    .subscribe( (response: any) => {
                            if (response.isSuccesFully){
                              this.formUser.reset();
                              this.toast.Success('¡Se guardo con exito!');
                              this.onClose();
                            }
                            else {
                              this.toast.Success(`Ocurrio un error ${ToolsHelper.getDataError(response.errors)}`);
                            }
                          }, (error) => {
                              console.log(error);
                              this.toast.Warn(`Ocurrio un error ${ToolsHelper.getDataError(error)}`);
                          });
  }

  private UpdateUser(): void {
    this.userService.putPersistenceUser(this.formUser.value)
                    .subscribe( (response: any) => {
                            if (response.isSuccesFully){
                              this.formUser.reset();
                              this.toast.Success('¡Se guardo con exito!');
                              this.onClose();
                            }
                            else{
                              this.toast.Success(`Ocurrio un error ${ToolsHelper.getDataError(response.errors)}`);
                            }
                          }, (error) => {
                            this.toast.Warn(`Ocurrio un error ${ToolsHelper.getDataError(error)}`);
                          });
  }

  public errorHandling = (control: string, error: string) => {
    return this.formUser.controls[control].hasError(error);
  }

  private onClose(): void {
    this.formUser.reset();
    this.dialogRef.close();
  }
}
