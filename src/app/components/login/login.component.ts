import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToolsHelper } from '../../../helper/ToolsHelper';
import { AuthService } from '../../../services/Auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;
  isInvalid: boolean;
  Msj: string;
  constructor(private form: FormBuilder,
              private router: Router,
              private authService: AuthService) {
    this.initControlForm();
  }
  ngOnInit(): void {
    this.isInvalid = false;
  }

  private initControlForm(): void {
    this.formLogin = this.form.group({
      parameters: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  public loginAuth(): void {
      this.goServicesProcessAuth();
  }

  private goServicesProcessAuth(): any {
    this.authService.postAuthLogin(this.formLogin.value)
                    .subscribe( (response: any) => {
                      localStorage.setItem('jwt', response.data.token);
                      this.router.navigate(['/usuarios']);
                    }, (error) => {
                      this.isInvalid = true;
                      this.Msj = ToolsHelper.getDataError(error);
                    });
  }

  public errorHandling = (control: string, error: string) => {
    return this.formLogin.controls[control].hasError(error);
  }
}
