import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './components/material/material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';

/*Security */
import { JwtModule } from '@auth0/angular-jwt';
/*Security */

/*AuthGuard */
import { AuthGuard } from '../guards/AuthGuard';
/*AuthGuard */

/*Modulos */
import { PipesModule } from '../pipes/pipes.module';
/*Modulos */

/* Services */
import { HttpClientModule } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/Auth/auth.service';
/* Services */



/*Shared Components */
import { ToolbarComponent } from './components/shared/toolbar/toolbar.component';
import { HomeComponent } from './components/shared/home/home.component';
import { LayoutComponent } from './components/shared/layout/layout.component';
import { SidenavComponent } from './components/shared/sidenav/sidenav.component';
import { LoginComponent } from './components/login/login.component';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { UserComponent } from './components/users/user/user.component';
import { UserAddComponent } from './components/users/user-add/user-add.component';
/*Shared Components */

/* jwt getToken */
export function tokenGetterLocal(): string {
  return localStorage.getItem('jwt');
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    LayoutComponent,
    SidenavComponent,
    LoginComponent,
    ListUsersComponent,
    UserComponent,
    UserAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    PipesModule,
    MatSnackBarModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetterLocal,
        whitelistedDomains: ['localhost:44357' ],
        blacklistedRoutes: []
      }
    }),
  ],
  providers: [
    AuthGuard,
    AuthService,
    UserService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
