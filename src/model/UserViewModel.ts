export class UserViewModel{
    userId: string;
    authId: string;
    userName: string;
    email: string;
    sex: string;
    status: boolean;
    typeOperation: string;
}
