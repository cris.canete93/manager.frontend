export class PageEntityViewModel<T>{
    totalPages: number;
    records: T[];
    currentPage: number;
    totalRecords: number;
}
