import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, Subject, throwError } from 'rxjs';
import { Response } from '../model/Response';
import { PageEntityViewModel } from '../model/PageEntityViewModel';
import { UserViewModel } from '../model/UserViewModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = environment.baseUrlAPI;
  public isReload$: Subject<boolean>;

  constructor(private http: HttpClient) {
    this.isReload$ = new Subject<boolean>();
  }

    getIsValidUser(email: string): Observable<Response<boolean>> {
      return this.http.get(`${this.baseUrl}Users/GetValidEmail?email=${email}`, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }).pipe(map((response: Response<boolean>) => {
          return response;
      }), catchError((exeptionError: HttpErrorResponse) => {
          return throwError(exeptionError.error);
      }));
    }

    persistenceUser(user: any): Observable<Response<boolean>> {
      const contentJson = JSON.stringify(user);
      return this.http.post(`${this.baseUrl}Users`, contentJson, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }).pipe(map((response: Response<boolean>) => {
          this.isReload$.next(true);
          return response;
      }), catchError((exeptionError: HttpErrorResponse) => {
          return throwError(exeptionError.error);
      }));
    }

    putPersistenceUser(user: any): Observable<Response<boolean>> {
      const contentJson = JSON.stringify(user);
      return this.http.put(`${this.baseUrl}Users`, contentJson, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }).pipe(map((response: Response<boolean>) => {
          this.isReload$.next(true);
          return response;
      }), catchError((exeptionError: HttpErrorResponse) => {
          return throwError(exeptionError.error);
      }));
    }


    getAllUser(page: number): Observable<PageEntityViewModel<UserViewModel>> {
      return this.http.get(`${this.baseUrl}Users?page=${page}`, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }).pipe(map((response: Response<PageEntityViewModel<UserViewModel>>) => {
          return response.data;
      }), catchError((exeptionError: HttpErrorResponse) => {
          return throwError(exeptionError.error);
      }));
    }

    changeStatus(user: UserViewModel): Observable<Response<boolean>> {
      return this.http.delete(`${this.baseUrl}Users/${user.userId}`, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }).pipe(map((response: Response<boolean>) => {
          return response;
      }), catchError((exeptionError: HttpErrorResponse) => {
          return throwError(exeptionError.error);
      }));
    }
}
