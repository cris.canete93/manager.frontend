import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, Subject, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl = environment.baseUrlAPI;
  private isAuth$: Subject<boolean>;

  constructor(private http: HttpClient) {
    this.isAuth$ = new Subject<boolean>();
  }

  postAuthLogin(auth: any): any {
    const contentJson = JSON.stringify(auth);
    return this.http.post(`${this.baseUrl}Auth/Login`, contentJson, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).pipe( map( (response: any) => {
      this.isAuth$.next(response.isSuccess);
      return response;
    }), catchError( (exeptionError: any) => {
      return throwError(exeptionError.error);
    }));
  }

  isCurrentAuthentication$(): Observable<boolean>{
   return this.isAuth$.asObservable();
  }
}
